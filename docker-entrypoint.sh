#!/bin/ash
set -e

if [ $# -eq 0 ] || [ "abuild" == "$(basename $1)" ]; then
  color_red='\033[1;31m'
  color_cyan='\033[1;36m'
  color_reset='\033[0m'

  public_keys=$(find /abuild/.abuild -type f -name "*.pub")
  if [ ! -z "${public_keys}" ]; then
    sudo cp /abuild/.abuild/*.pub /etc/apk/keys/
  else 
    echo -e "${color_red}No signing and verification key pair is found.${color_reset}"
  fi

  # Make sure that we have all the needed directories with ownership
  echo -e "${color_cyan}Fixing directory ownership and permission...${color_reset}"
  sudo chgrp $(id -g) "/v${ALPINE_VERSION}" && sudo chmod g+w "/v${ALPINE_VERSION}"
  sudo chgrp $(id -g) /var/cache/distfiles && sudo chmod g+w /var/cache/distfiles
  sudo chown root:root /root/.cache/pip
  sudo chgrp $(id -g) /abuild/.composer/cache && sudo chmod g+w /abuild/.composer/cache

  sudo chgrp $(id -g) /main && sudo chmod g+w /main
  find /main -maxdepth 1 -mindepth 1 -type d -not -name ".*" -exec sudo chgrp $(id -g) {} \; -exec sudo chmod g+w {} \;
  find /main -type f -name "APKBUILD" -exec sudo chgrp $(id -g) {} \; -exec sudo chmod g+w {} \;
  #find /main -type f -name "src" -exec sudo chown -R $(id -u):$(id -g) {} \;
  #find /main -type f -name "pkg" -exec sudo chown -R $(id -u):$(id -g) {} \;

  # Make sure the repository indexes are up to date
  echo -e "${color_cyan}Updating indexes from all remote repositories...${color_reset}"
  abuild-apk update

  # Run zsh by default
  if [ $# -eq 0 ]; then
    echo -e "${color_cyan}Welcome to APKBUILD!${color_reset}"
    exec zsh
  fi
fi

exec "$@"
