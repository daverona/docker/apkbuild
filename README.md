# docker/apkbuild

[![pipeline status](https://gitlab.com/daverona/docker/apkbuild/badges/master/pipeline.svg)](https://gitlab.com/daverona/docker/apkbuild/-/commits/master)

This is a repository of Docker images for Alpine APK package building.

* GitLab repository: [https://gitlab.com/daverona/docker/apkbuild](https://gitlab.com/daverona/docker/apkbuild)
* Docker registry: [https://hub.docker.com/r/daverona/apkbuild](https://hub.docker.com/r/daverona/apkbuild)
* Avaliable releases: [https://gitlab.com/daverona/docker/apkbuild/-/releases](https://gitlab.com/daverona/docker/apkbuild/-/releases)

## Quick Start

There is a set of scripts to make your life easier. First get them:

```bash
docker container run --rm daverona/apkbuild cat /abuild/scripts/keygen.sh > keygen.sh
docker container run --rm daverona/apkbuild cat /abuild/scripts/buildenv.sh > buildenv.sh
docker container run --rm daverona/apkbuild cat /abuild/scripts/plainenv.sh > plainenv.sh
chmod a+x *.sh
```

Intialize cache directories and generate signing and verification key pair for abuild:

```bash
ABUILD_EMAIL=abuild@localhost ./keygen.sh
```

Observe on the host the following directories:

* `~/.abuild/keys`: key directory -- *Back them up, please*
* `~/.abuild/apk`: (*cache*) downloaded APK files required to build yours
* `~/.abuild/distfiles`: (*cache*) downloaded source files required to build your packages
* `~/.abuild/pip`: (*cache*) downloaded and built Python modules required to build your packages
* `~/.abuild/composer`: (*cache*) downloaded PHP packages required to build your packages
* `${PWD}/v3.12`: generated package repository

The *cache* directories are to avoid repeated downloading and building dependencies.

Run a container in building environment:

```bash
./buildenv.sh
```

Observe in the container the following directories:

* `/abuild/.abuild` &mdash; key directory
* `/main` &mdash; working directory
* `/v3.12` &mdash; generated package repository

Start a new APKBUILD and build APK files:

```bash
cd /main
newapkbuild mine
cd mine

# edit APKBUILD

abuild checksum
abuild -r -K
```

If there's no error, APK files will be generated under `/v3.12/main/<arch>` directory in the container,
where `<arch>` is your host architecture you can get with command `arch`.

Before distribute these APK files,
run another container in non-building environment to test-drive them:

```bash
./plainenv.sh

# in the container
cd /v3.12/main/<arch>
apk add *.apk
```

When you distribute your APK files, you should give abuild's public key too,
which is under `~/.abuild/keys/*.rsa.pub` on the host. 
Say it is `abuild@localhost-4a6a0840.rsa.pub`.
Then others may run these commands to install APK files:

```bash
cp abuild@localhost-4a6a0840.rsa.pub /etc/apk/keys
apk add *.apk
```

It's safe to remove APK files and the public key after installation.

## Usage

## References

* Creating an Alpine Package: [https://wiki.alpinelinux.org/wiki/Creating\_an\_Alpine\_package](https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package)
* Creating an APKBUILD file: [https://wiki.alpinelinux.org/wiki/Creating\_an\_Alpine\_package#Creating\_an\_APKBUILD\_file](https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package#Creating_an_APKBUILD_file).
* Build the package: [https://wiki.alpinelinux.org/wiki/Creating\_an\_Alpine\_package#Build\_the\_package](https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package#Build_the_package).
* APKBUILD Reference: [https://wiki.alpinelinux.org/wiki/APKBUILD\_Reference](https://wiki.alpinelinux.org/wiki/APKBUILD_Reference)
* APKBUILD examples: [https://wiki.alpinelinux.org/wiki/APKBUILD\_examples](https://wiki.alpinelinux.org/wiki/APKBUILD_examples)
* aports repository: [https://gitlab.alpinelinux.org/alpine/aports](https://gitlab.alpinelinux.org/alpine/aports)
* dnephin/docker-apk-build: [https://github.com/dnephin/docker-apk-build](https://github.com/dnephin/docker-apk-build)
* andyshinn/docker-alpine-abuild: [https://github.com/andyshinn/docker-alpine-abuild](https://github.com/andyshinn/docker-alpine-abuild)

