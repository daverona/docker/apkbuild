#!/bin/bash
set -e

ALPINE_VERSION=${ALPINE_VERSION:-{{alpine-version}}}
ABUILD_EMAIL=${ABUILD_EMAIL:-abuild@localhost}

mkdir -p \
  "${HOME}/.abuild/keys" \
  "${HOME}/.abuild/apk" \
  "${HOME}/.abuild/composer" \
  "${HOME}/.abuild/pip" \
  "${HOME}/.abuild/distfiles" \
  "${PWD}/v${ALPINE_VERSION}"

docker container run --rm \
  --volume "${HOME}/.abuild/keys:/abuild/.abuild" \
  daverona/apkbuild:${ALPINE_VERSION} \
    zsh -c "\
      git config --global user.email ${ABUILD_EMAIL} \
      && sudo chgrp \$(id -g) /abuild/.abuild \
      && sudo chmod g+w /abuild/.abuild \
      && abuild-keygen -a -n \
    "
