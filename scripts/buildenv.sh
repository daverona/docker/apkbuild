#!/bin/bash
set -e

ALPINE_VERSION=${ALPINE_VERSION:-{{alpine-version}}}

docker container run --rm \
  --interactive --tty \
  --volume "${HOME}/.abuild/keys:/abuild/.abuild" \
  --volume "${HOME}/.abuild/apk:/var/cache/apk" \
  --volume "${HOME}/.abuild/composer:/abuild/.composer/cache" \
  --volume "${HOME}/.abuild/pip:/root/.cache/pip" \
  --volume "${HOME}/.abuild/distfiles:/var/cache/distfiles" \
  --volume "${PWD}/v${ALPINE_VERSION}:/v${ALPINE_VERSION}" \
  --volume "${PWD}:/main" \
  daverona/apkbuild:${ALPINE_VERSION} "$@"
