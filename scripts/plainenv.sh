#!/bin/bash
set -e

ALPINE_VERSION=${ALPINE_VERSION:-{{alpine-version}}}

if [ -z "${ABUILD_KEYNAME}" ]; then
  public_keys=$(find ${HOME}/.abuild/keys -type f -name "*.pub")
  if [ -z "${public_keys}" ]; then
    ABUILD_KEYNAME=abuild
  else
    for public_key in $public_keys; do break; done
    ABUILD_KEYNAME=$(echo $(basename ${public_key}) | sed -r "s|^(.*).rsa.pub|\1|")
  fi
fi
if [ ! -f "${HOME}/.abuild/keys/${ABUILD_KEYNAME}.rsa.pub" ]; then
  color_red='\033[1;31m'
  color_reset='\033[0m'
  echo -e "${color_red}No verification key is found.${color_reset}"
  exit 1
fi 

docker container run --rm \
  --interactive --tty \
  --volume "${HOME}/.abuild/keys/${ABUILD_KEYNAME}.rsa.pub:/etc/apk/keys/${ABUILD_KEYNAME}.rsa.pub:ro" \
  --volume "${PWD}/v${ALPINE_VERSION}:/v${ALPINE_VERSION}:ro" \
  --workdir "/v${ALPINE_VERSION}" \
  alpine:${ALPINE_VERSION} "$@"
