ARG ALPINE_VERSION=3.12
FROM alpine:$ALPINE_VERSION

ARG ALPINE_VERSION
ENV ALPINE_VERSION=${ALPINE_VERSION}
RUN apk add --no-cache \
    alpine-sdk \
    sudo \
    tzdata \
    vim \
    zsh \
  # Replace default vi with vim
  && ln -sf /usr/bin/vim /usr/bin/vi \
  # Enable distfiles cache which keeps downloaded source tar.gz
  # @see https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package#Setup_your_system_and_account
  && mkdir -p /var/cache/distfiles \
  && chgrp abuild /var/cache/distfiles \
  && chmod g+w /var/cache/distfiles \
  # Enable local cache which keeps downloaded APKs
  # @see https://wiki.alpinelinux.org/wiki/Local_APK_cache#Enabling_Local_Cache_on_HDD_installs
  && mkdir -p /var/cache/apk \
  && ln -s /var/cache/apk /etc/apk/cache \
  # Make pip cache (for root)
  && mkdir -p /root/.cache/pip \
  # Create a user account "abuild"
  && adduser -D -s /bin/zsh -h /abuild -G abuild -u $(getent group abuild | sed -r "s|^.*:(.*)$|\1|") abuild \
  && echo "abuild ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers \
  # Make composer cache (for abuild)
  && mkdir -p /abuild/.composer/cache \
  && chown -R abuild:abuild /abuild/.composer/cache \
  # Do something nice(?) for "abuild" account
  && echo "PROMPT='%F{yellow}%n%f@%F{blue}%m%f:%F{cyan}%~%f %F{red}%(!.#.$)%f '" > /abuild/.zshrc \
  && { \
    echo "set nocompatible"; \
    echo "filetype indent plugin on"; \
    echo "syntax on"; \
    echo "set hidden"; \
    echo "set wildmenu"; \
    echo "set showcmd"; \
    echo "set hlsearch"; \
    echo "set ignorecase"; \
    echo "set smartcase"; \
    echo "set backspace=indent,eol,start"; \
    echo "set autoindent"; \
    echo "set nostartofline"; \
    echo "set ruler"; \
    echo "set laststatus=2"; \
    echo "set confirm"; \
    echo "set t_vb="; \
    echo "set cmdheight=2"; \
    echo "set number"; \
    echo "set notimeout ttimeout ttimeoutlen=200"; \
    echo "set pastetoggle=<F11>"; \
    echo "set shiftwidth=2"; \
    echo "set softtabstop=2"; \
    echo "set expandtab"; \
    echo "set showmatch"; \
    echo "set nowrap"; \
    echo "colorscheme desert"; \
  } > /abuild/.vimrc \
  # Make directories for the user
  && mkdir -p /abuild/.abuild "/v${ALPINE_VERSION}" /main \
  && chown -R abuild:abuild /abuild "/v${ALPINE_VERSION}" /main

COPY scripts/ /abuild/scripts/
RUN true \
  # Customize scripts
  && sed -i "s|{{alpine-version}}|$ALPINE_VERSION|" /abuild/scripts/buildenv.sh \
  && sed -i "s|{{alpine-version}}|$ALPINE_VERSION|" /abuild/scripts/plainenv.sh \
  && sed -i "s|{{alpine-version}}|$ALPINE_VERSION|" /abuild/scripts/keygen.sh

# Configure miscellanea
COPY docker-entrypoint.sh /
RUN chmod a+x /docker-entrypoint.sh
WORKDIR /main
USER abuild

ENV REPODEST="/v${ALPINE_VERSION}"

ENTRYPOINT ["/docker-entrypoint.sh"]
